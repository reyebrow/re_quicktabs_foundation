<?php
/**
 * @todo: Remove the coupling with Cogito module.
 *        Allow for the use of Libraries module to detect Foundation.
 *        Make sure that class names are sanitized:
 *        strtolower(preg_replace('/[^a-zA-Z0-9-]+/', '-', $original));
 */

/**
 * Renders the content using the Foundation Tabs widget.
 */
class QuickFoundationTabs extends QuickRenderer {
  public static function optionsForm($qt) {
    $form = array();
    $form['tabtype'] = array(
      '#options' => array (
        '' => t('Simple'),
        'contained' => t('Contained'),
        'pill' => t('Pill-Style'),
      ),
      '#type' => 'radios',
      '#title' => t('Type'),
      '#description' => t('Choose the style of tab to use from ' . l('Foundation', 'http://foundation.zurb.com/docs/tabs.php#simpleContained1')),
      '#default_value' => (isset($qt->renderer) && $qt->renderer == 'foundation_tabs' && isset($qt->options['tabtype']) && $qt->options['tabtype']) ? $qt->options['tabtype'] : '',

    );
    $form['tabsizing'] = array(
      '#type' => 'textfield',
      '#title' => 'Tab Sizing Class',
      '#description' => t('If you want your tabs to run the full width of their container evenly, you can add class of <em>two-up</em>, <em>three-up</em>, <em>four-up</em>, and <em>five-up</em> to them.'),
      '#default_value' => (isset($qt->renderer) && $qt->renderer == 'foundation_tabs' && isset($qt->options['tabsizing']) && $qt->options['tabsizing']) ? $qt->options['tabsizing'] : '',
    );

    $form['orientation'] = array(
      '#type' => 'fieldset',
      '#title' => t('Vertical Tabs'),
      '#collapsible' => TRUE,       
      '#collapsed' => !(isset($qt->renderer) && $qt->renderer == 'foundation_tabs' && isset($qt->options['orientation']['vertical']) && $qt->options['orientation']['vertical']),
    );

    $form['orientation']['vertical'] = array(
      '#type' => 'checkbox',
      '#title' => 'Vertical Tabs',
      '#description' => t('Use the above Type but make it a Vertical tab list.'),
      '#default_value' => (isset($qt->renderer) && $qt->renderer == 'foundation_tabs' && isset($qt->options['orientation']['vertical']) && $qt->options['orientation']['vertical']),
    );

    $form['orientation']['tabset'] = array(
      '#type' => 'textfield',
      '#title' => 'Tabset class',
      '#description' => t('Add a Foundation class or other class to the Tabset wrapper (&lt;dl&gt;).'),
      '#default_value' => (isset($qt->renderer) && $qt->renderer == 'foundation_tabs' && isset($qt->options['orientation']['tabset']) && $qt->options['orientation']['tabset']) ? $qt->options['orientation']['tabset'] : '',
    );

    $form['orientation']['tabcontent'] = array(
      '#type' => 'textfield',
      '#title' => 'Tab content class',
      '#description' => t('Add a Foundation class or other class to the Tab content wrapper (&lt;ul&gt;).'),
      '#default_value' => (isset($qt->renderer) && $qt->renderer == 'foundation_tabs' && isset($qt->options['orientation']['tabcontent']) && $qt->options['orientation']['tabcontent']) ? $qt->options['orientation']['tabcontent'] : '',
    );

    return $form;
  }

  public function render() {
    $quickset = $this->quickset;
    $settings = $this->quickset->getSettings();
    $options = $settings['options'];
    
    // If vertical orientation is enabled, set the appropriate variables to use in the theming functions...
    if($options['orientation']['vertical'] == 1) {
      $options['tabtype'] .= ' vertical';
      $tabset_width = $options['orientation']['tabset'];
      $tabcontent_width = $options['orientation']['tabcontent'];
    }
    
    $active_tab = $quickset->getActiveTab();
    $tabs = $this->build_tablinks($active_tab);
    $qt_name = $quickset->getName();
    $render_array = array(
      'content' => array(
        '#theme' => 'qt_foundation_tabs',
        '#options' => array('attributes' => array(
          'id' => 'quicktabs-' . $qt_name,
          'class' => 'quicktabs-foundation-wrapper',
        )),
        'tabs' => array('#theme' => 'qt_foundation_tabs_tabset', '#options' => array('active' => $active_tab, 'type' => $options['tabtype'], 'tabset_width' => (isset($tabset_width)) ? $tabset_width : NULL, 'tabcontent_width' => (isset($tabcontent_width)) ? $tabcontent_width : NULL, 'tabsizing' => (isset($options['tabsizing'])) ? $options['tabsizing'] : NULL), 'tablinks' => $tabs),
        'lis' => array(),
      ),
    );

    /* Tab content */
    foreach ($quickset->getContents() as $key => $tab) {
      if (!empty($tab)) {
        $attribs = array(
          // Foundation also requires the word Tab appended to the content list item id...
          'id' => 'qt-'. $qt_name .'-foundation-tabs' . ($key+1) . 'Tab', 
        );
        if ($key == $active_tab) { 
          $attribs['class'][] = 'active';
        }
        // Foundation tabs put the active class right on the list item...
        $render_array['content']['lis'][] = array(
          '#prefix' => '<li '. drupal_attributes($attribs) . '>',
          '#suffix' => '</li>',
          'content' => $tab->render(),
        );
      }
    }
    return $render_array;
  }

  
  /**
   * Build the actual tab links, with appropriate href, title and attributes.
   * 
   * @param $active_tab The index of the active tab.
   */
  protected function build_tablinks($active_tab) {
    $tabs = array();
    $qt_name = $this->quickset->getName();
    foreach ($this->quickset->getContents() as $i => $tab) {
      if (!empty($tab)) {
        $classes = array();
        // Build an attributes array
        $attribs = array(
          // Foundation also requires the word Tab appended to the content list item id...
          'href' => '#qt-'. $qt_name .'-foundation-tabs' . ($i+1),
          'class' => $classes,
        );

        $markup = '<a '. drupal_attributes($attribs) . '>' . check_plain($this->quickset->translateString($tab->getTitle(), 'tab', $i)) .'</a>';
        $tablink = array(
          '#markup' => $markup,
        );
        $tabs[$i] = $tablink;
      }
    }
    return $tabs;
  }
}
